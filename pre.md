pre
========================================================
author: 
date: 




First Slide
========================================================

For more details on authoring R presentations click the
**Help** button on the toolbar.

- Bullet 1
- Bullet 2
- Bullet 3

Slide With Code
========================================================


```r
summary(cars)
```

```
     speed           dist    
 Min.   : 4.0   Min.   :  2  
 1st Qu.:12.0   1st Qu.: 26  
 Median :15.0   Median : 36  
 Mean   :15.4   Mean   : 43  
 3rd Qu.:19.0   3rd Qu.: 56  
 Max.   :25.0   Max.   :120  
```

Slide With Plot
========================================================


```
<iframe src=' pre-figure/unnamed-chunk-3.html ' scrolling='no' frameBorder='0' seamless class='rChart nvd3 ' id=iframe- chart1047293952a0 ></iframe> <style>iframe.rChart{ width: 100%; height: 400px;}</style>
```
